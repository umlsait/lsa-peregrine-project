$(document).ready(function () {  
    //Using this setTimeout work around allows some flexibility in ordering.
    //It is especially useful for editable template pages.
    setTimeout(registerHover, 50)
  });
  
  var registerHover = function() {
    $(".audience-nav-item .audItem").hoverIntent( function() {
        const name = $(this).attr('data-hover');
        if ($('#audMenu-' + name).hasClass('open')) {
            // Do nothing
        } else {
            $('.level-two-nav.open').slideToggle('fast').removeClass('open');
            $('#audMenu-' + name).addClass('open').slideToggle('fast');
        }
    });
  
    $(".audience-nav-item .audItem").focus ( function() {
        const $audienceL1 = $(this);
        const name = $audienceL1.attr('data-hover');
        if ($('#audMenu-' + name).hasClass('open')) {
            // Do nothing
        } else {
            $('.level-two-nav.open').slideToggle('fast').removeClass('open');
            $('#audMenu-' + name).addClass('open').slideToggle('fast');
        }
        $(this).keydown(function(e) {
          const name = $(this).attr('data-hover');
          if (e.keyCode == 40 ) { // down arrow enters
            e.preventDefault();
            $('#audMenu-'+name+" a").first().focus();
          }
        });
        $('#audMenu-' + name).keydown(function(e) {
          if (e.keyCode == 38 ) { // up arrow sets focus back to the top level item
            e.preventDefault();
            $audienceL1.focus();
          }            
        });      
    });
  
    $(".department-nav-hoverzone").hoverIntent( function() {}, function() {
      $('.level-two-nav.open').slideToggle('fast').removeClass('open');
    });
    
    
  
    $(".audience-nav-item a.audItem").click( function() {
        if ( Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 992 ) {
              var name = $(this).attr('data-hover');
          if ( $('#audMenu-' + name).hasClass('open') == false && $('#audMenu-' + name).length != 0 ) {
                event.preventDefault();
                $('.level-two-nav.open').slideToggle('fast').removeClass('open');
                $('#audMenu-' + name).addClass('open').slideToggle('fast');
            }
        }
    });
  
    // if (!Modernizr.svg) {
    //   $(".header-wrap .fallback").show();
    // }  
  }