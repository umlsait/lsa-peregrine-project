package edu.umich.lsa.peregrine.core.common

class Link {
    def href = ""
    boolean external = false
    def linkedText = ""
    def titleAttr = ""
    def target = "_top"
}
