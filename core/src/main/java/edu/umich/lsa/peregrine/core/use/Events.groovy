package edu.umich.lsa.peregrine.core.use

import com.peregrine.adaption.PerPage
import com.peregrine.pagerender.server.helpers.BaseHelper
import edu.umich.lsa.peregrine.core.events.Event
import edu.umich.lsa.peregrine.core.events.Image
import edu.umich.lsa.peregrine.core.events.Metadata
import org.apache.sling.api.resource.ValueMap
import edu.umich.lsa.peregrine.core.helpers.HttpHelper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.sling.settings.SlingSettingsService;
import javax.script.Bindings;
import com.google.gson.*
import java.time.Duration
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime;

public class Events extends BaseHelper {
    private static final String EVENTS_BASE_URL = "http://events.umich.edu/group/%s/json?v=2&html_output=true";
    //was "http://events.umich.edu/list/json?filter=sponsors:%s"
    private static final String METADATA_BASE_URL = "http://events.umich.edu/group/metadata/%s/json";
    private static final Logger log = LoggerFactory.getLogger(Events.class);

    private String eventPage;
    private String featuredTag;
    private String[] tags;
    private String sponsorId;
    private int maxResults;
    private List<Event> events;
    private Event featuredEvent;
    private Metadata metadata;
    private Boolean onHomepage = false;
    private SlingSettingsService slingSettings;
    private Set<String> runmodes;

    @Override
    void init(Bindings bindings) {
        super.init(bindings);
        setComponentProperties();
        slingSettings = sling.getService(SlingSettingsService.class);
        runmodes = slingSettings.getRunModes();
//        this.processEvents();
    }

    static ZoneId getZoneId() {
        return ZoneId.of( "America/New_York" );
    }

    static ZonedDateTime getCurrentTime() {
        return ZonedDateTime.now( getZoneId() );
    }

    static Long computeSecondsUntilMidnight(ZonedDateTime now ){
        LocalDate tomorrow = now.toLocalDate().plusDays(1);
        ZonedDateTime tomorrowStart = tomorrow.atStartOfDay( Events.getZoneId() );
        Duration duration = Duration.between( now , tomorrowStart );
        return duration.getSeconds();
    }

    protected void setComponentProperties() {
        // 1. Set sponsor ID. give preference to cosponsors, other get the hp sponsor id
        String sponsorStr = this.getProperties().get("cosponsor", "");
        if (sponsorStr.equals("")){
//            this.getCurrentPage().getSiteProperties()
            PerPage page = getCurrentPage()
            ValueMap siteProps = getCurrentPage().getSiteProperties()
            sponsorStr = siteProps.get("eventSponsorId","");
        }
        this.setSponsorId(sponsorStr.trim()); //this.getInheritedProperties().get("eventSponsorId","")

        // 2. Set the event page URL. This is where the main event calendar is
        //  This is also used to build event detail page URL's
        this.setEventPage(this.getCurrentPage().getSiteProperties().get("eventPageUrl",""));

    }

    protected void processEvents() {
        events = new ArrayList<Event>();
        String json = "";
        try {
            json = HttpHelper.GetHttpResponse(this.getFullUrl());
            if(json.length() <4){ // [ ], or  []
                log.debug("No Events, json="+json);
                return;
            }
            JsonParser parser = new JsonParser();
            JsonArray data = parser.parse(json).getAsJsonArray();
            Gson gson = new GsonBuilder().create();
            Set<String> multiDayEventSet = new HashSet<String>();
            for (JsonElement entry : data) {
                Event eventEntry = gson.fromJson(entry, Event.class);
                String uniqueEventId = eventEntry.getId();
                String eventId = uniqueEventId.substring(0, uniqueEventId.indexOf("-"));
                // if no featured event yet (its null), then check if this event is featured
                if(featuredEvent == null && isFeatured(eventEntry)){  //&& hasImage(eventEntry)
                    featuredEvent = eventEntry;
                    if(eventEntry.getOccurrence_count() > 1){
                        multiDayEventSet.add(eventId);
                    }
                }
                else if (eventEntry.getOccurrence_count() > 1) {
                    if (!multiDayEventSet.contains(eventId)) {
                        multiDayEventSet.add(eventId);
                        events.add(eventEntry);
                    }
                }
                else {
                    events.add(eventEntry);
                }
            }
            if(featuredEvent == null) {
                processFeaturedEvent(null);
            }
        } catch(Exception e) {
            log.error("Events HTTP JSON ERROR", e);
        }
    }

    private Boolean isFeatured(Event eventToCheck){
        if(featuredTag == null){
            log.debug("There was no Featured Tag");
            return false;
        } else if(onHomepage == false && hasImage(eventToCheck)==false) {
            // if not homepage then check for image
            log.debug("not on home page "+onHomepage);
            log.debug("has image "+ hasImage(eventToCheck));
            return false;
        } else{
            log.debug("on home page with featured tag, "+this.featuredTag);
        }
        List<String> tags = eventToCheck.getTags();     // gets the event tags
        int i =0;
        for(String tagString : tags){
            tags.set(i, tagString.toLowerCase());
            i++;
        }
        String featuredLwr = getFeaturedTag().toLowerCase();
        if(tags !=null && tags.size() != 0 && tags.contains(featuredLwr) ){
            // iterate thru and compare using equalsIgnore case
            return true;
        } else {
            return false;
        }
    }

    private Boolean hasImage(Event eventToCheck){
        Image image = eventToCheck.getStyled_images();
        if (image != null && image.getEvent_large_2x() != null && !image.getEvent_large_2x().isEmpty()) {
            //data.entrySet().remove(tempEvent);    // Remove featured event from events
            return true;
        } else {
            return false;
        }
    }

    protected void processMetadata() {
        String json = "";
        try {
            json = HttpHelper.GetHttpResponse(this.getMetadataUrl());
            JsonParser parser = new JsonParser();
            JsonObject data = parser.parse(json).getAsJsonObject();
            Gson gson = new GsonBuilder().create();
            this.setMetadata(gson.fromJson(data, Metadata.class));
            log.debug("***\n***\n*** Tags Size: " + this.metadata.getTags().size());
        } catch(Exception e) {
            log.error("Events JSON Parse error",e);
        }
    }

    protected void processFeaturedEvent(String[] categoryTags) {
        String json = "";
        if (!categoryTags) {
            categoryTags = new String()[]
        }

        try {
            String featuredUrl = this.getFeaturedUrl();
            if(featuredUrl.endsWith(":")){
                return;
            }
            json = HttpHelper.GetHttpResponse(this.getFeaturedUrl());
            if(json.length() <4){ // [ ], or  []
                log.debug("No Featured Event to Process, json="+json);
                return;
            }
            JsonParser parser = new JsonParser();
            JsonArray data = parser.parse(json).getAsJsonArray();
            Gson gson = new GsonBuilder().create();
            // find first featured event with an image
            for (JsonElement entry : data) {
                Event tempEvent = gson.fromJson(entry, Event.class);
                List<String> tags = tempEvent.getTags();
                if(!tags.contains(featuredTag))
                {
                    continue;
                }
                Image image = tempEvent.getStyled_images();
                if (image != null && image.getEvent_large_2x() != null && !image.getEvent_large_2x().isEmpty()) {
                    boolean featuredEventIndicator = (categoryTags.length == 0) ? true : false;
                    for (String categoryTag : categoryTags) {
                        if(tags.contains(categoryTag)) {
                            featuredEventIndicator = true;
                            break;
                        }
                    }
                    if(featuredEventIndicator) {
                        featuredEvent = tempEvent;
                        return;
                    }
                }
            }
        } catch(Exception e) {
            log.error("failed to process featured event ",e);
        }
    }

    public String getUrl() {
        // http://events.umich.edu/group/stats/json?
        return String.format(EVENTS_BASE_URL, this.getSponsorId());
    }

    public String getRss(){
        return getUrl().replaceFirst("json","rss");
    }

    public String getUrl(String cosponsor) {
        if(cosponsor == null || cosponsor.isEmpty()){
            return getUrl();
        } else {
            return String.format(EVENTS_BASE_URL, cosponsor);
        }
    }

    private String getFullUrl() {
        //http://events.umich.edu/group/stats/json?max-results=3
        return String.format("%s&max-results=%s", this.getUrl(), 20);
    }

    protected String getFeaturedUrl() throws UnsupportedEncodingException {
        //http://events.umich.edu/group/stats/json?filter=tags:Featured
        return String.format("%s&filter=tags:%s", this.getUrl(), URLEncoder.encode(this.getFeaturedTag(), "UTF-8"));
    }

    protected String getMetadataUrl() {
        return String.format(METADATA_BASE_URL, this.getSponsorId());
    }

    protected String getTagQuery(String[] categoryTags) {
        return getTagQuery(categoryTags, null);
    }

    protected String getTagQuery(String[] categoryTags, String featuredTag) {
        StringBuilder tags = new StringBuilder();
        try {
            if (featuredTag != null && !featuredTag.isEmpty()) {
                if (categoryTags != null && categoryTags.length > 0) {
                    tags.append(URLEncoder.encode(featuredTag, "UTF-8")).append(",");
                } else {
                    tags.append(URLEncoder.encode(featuredTag, "UTF-8"));
                }
            }
        } catch (Exception ex) {
            log.error("An error occured encoding a URL value: {}", ex);
        }

        try {
            boolean first = true;
            for(String tag : categoryTags) {
                if (first) {
                    first = false;
                } else {
                    tags.append(",");
                }
                tags.append(URLEncoder.encode(tag, "UTF-8"));
            }
        } catch (Exception ex) {
            log.error("An error occured encoding a URL value: {}", ex);
        }
        return tags.toString(); // "featured,category1,category2"
    }

    public String getEventPage() {
        return eventPage;
    }

    public void setEventPage(String eventPage) {
        this.eventPage = eventPage;
    }

    public String getFeaturedTag() {
        return featuredTag;
    }

    public void setFeaturedTag(String featuredTag) {
        this.featuredTag = featuredTag.trim();
    }

    public Event getFeaturedEvent() {
        return featuredEvent;
    }

    public void setFeaturedEvent(Event featuredEvent) {
        this.featuredEvent = featuredEvent;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }
}
