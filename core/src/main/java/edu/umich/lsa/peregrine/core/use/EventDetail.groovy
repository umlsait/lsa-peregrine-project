package edu.umich.lsa.peregrine.core.use

import edu.umich.lsa.peregrine.core.events.Event
import javax.script.Bindings
import javax.servlet.http.HttpServletResponse;
import com.google.gson.*;
import edu.umich.lsa.peregrine.core.helpers.HttpHelper

class EventDetail extends Events {

    private static final String EVENTS_DETAIL_BASE_URL = "http://events.umich.edu/event/%s/feed/json?v=2&html_output=true";
    private static final String MULTIDAY_EVENT_URL = "http://events.umich.edu/event/%s/json?html_output=true";
    private static final String EVENTS_CACHE_KEY = "EventDetail";

    String eventID;
    Event event;
    Event multiDayEvent;
    int currentOccurrenceId;

    @Override
    void init(Bindings bindings) {
        super.init(bindings);
        this.eventID = this.processEventID();
        if (eventID.isEmpty()) {
            return;
        }
        /**
         * For event Detail, in order to pull the title into the template
         * and also show the details later, to avoid calling the API twice
         * we cache the result in a request attribute
         */
        Event cachedEvent = (Event) this.getRequest().getAttribute(EVENTS_CACHE_KEY);
        if (cachedEvent == null) {
            if (this.getUrl() != null && !this.getUrl().isEmpty()) {
                this.processEvent();
                this.getRequest().setAttribute(EVENTS_CACHE_KEY, this.event);
            }
        } else {
            event = cachedEvent;
        }
        if(event == null){
            this.setErrorEvent();
            return;
        }

        // Logic for multiday event
        if(event != null && event.getOccurrence_count() > 1) {
            String parsedEventId = eventID.substring(0, eventID.indexOf("-"));
            if(!parsedEventId.matches("\\d+")){
                this.setErrorEvent();
                return;
            }
            try {
                this.currentOccurrenceId = Integer.parseInt(eventID.substring(eventID.indexOf("-") + 1));
            } catch (NumberFormatException e) {
                log.error("NumberFormatException in getting Event ID from request suffix");
                this.setErrorEvent();
                return;
            }

            String json = "";
            try {
                json = HttpHelper.GetHttpResponse(String.format(MULTIDAY_EVENT_URL, parsedEventId));
                log.debug(json);
                JsonParser parser = new JsonParser();
                JsonObject data = parser.parse(json).getAsJsonObject();
                Gson gson = new GsonBuilder().create();
                this.multiDayEvent = gson.fromJson(data, Event.class);
            } catch(Exception e) {
                log.error(e.toString());
            }

            final List<Event> occurrences = multiDayEvent.getOccurrences();
            final Iterator<Event> iterator = occurrences.iterator();
            while (iterator.hasNext()) {
                final Event occurrence = iterator.next();
                if (occurrence.getOccurrence_id() != this.currentOccurrenceId) {
                    iterator.remove();
                }
                else {
                    break;
                }
            }
        }
    }


    private void setErrorEvent(){
        this.getResponse().setStatus(HttpServletResponse.SC_NOT_FOUND);
        event = new Event();
        event.setEvent_title("Event Not Found");
        event.setCombined_title("Event Not Found");
        event.setEvent_id(-1);
    }

    private String processEventID() {
        String suffix = this.getRequest().getRequestPathInfo().getSuffix();
        if (suffix != null) {
            suffix = suffix.replaceAll("/", "");
            if (suffix.contains(".")) {
                suffix = suffix.substring(0, suffix.indexOf('.'));
            }
            return suffix;
        }
        return "";
    }

    protected void processEvent() {

        String json = "";
        try {
            json = HttpHelper.GetHttpResponse(this.getFullUrl());
            log.debug(json);
            JsonParser parser = new JsonParser();
            JsonArray data = parser.parse(json).getAsJsonArray();
            Gson gson = new GsonBuilder().create();
            for (JsonElement entry : data) {
                this.event = gson.fromJson(entry, Event.class);
            }
        } catch(Exception e) {
            log.error(e.toString());
        }
    }

    private String getFullUrl() {
        return String.format(EVENTS_DETAIL_BASE_URL, this.getEventID());
    }
}
