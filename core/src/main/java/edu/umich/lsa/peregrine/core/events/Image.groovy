package edu.umich.lsa.peregrine.core.events

public class Image {
    String event_large_2x
    String event_grid_2x
    String event_thumb
    String event_large
    String event_large_lightbox
    String group_thumb
    String group_thumb_square
    String group_large
    String group_large_lightbox
    String event_large_crop
    String event_list
    String event_list_2x
    String event_grid
    String event_feature_large
    String event_feature_thumb
}