package edu.umich.lsa.peregrine.core.events

import com.google.gson.Gson
import edu.umich.lsa.peregrine.core.helpers.HttpHelper
import groovy.json.JsonException
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j

@Slf4j
class EventsAPIHelper {
    private static String METADATA_BASE = 'https://events.umich.edu/list/metadata/json?include_unpromoted'

    private static def metaDataUrl() {
        HttpHelper.makeUrl(METADATA_BASE, ['v': 2])
    }

    static def getEvents(sponsorID, List eventTag, maxResults=25, range="") {
        def eventsBase = 'https://events.umich.edu/group/' + sponsorID + '/json?v=2&html_output=true'
        def filteredParams = [];
        if (eventTag != null && !eventTag.isEmpty()) {
            String tags = eventTag.join(",")
            def encodedTag = URLEncoder.encode(tags, 'UTF-8')
            filteredParams = ['tags' : encodedTag]
        }
        def params = ['max-results': maxResults] + makeEventFilter(filteredParams)
        if (range) {
            params.put('range', range)
        }
        def eventsUrl = HttpHelper.makeUrl(eventsBase, params)
        if(eventTag?.size() > 1){
            return makeEventsWithAndTags(callApi(eventsUrl), eventTag)
        }

        return makeEvents(callApi(eventsUrl))
    }

    static def getEvent(eventID) {
        if (eventID == null || eventID == '') {
            return [] // early if we don't have an ID
        }
        def eventsUrl = "https://events.umich.edu/event/$eventID/feed/json?v=2&html_output=true".toURL()
        return makeEvents(callApi(eventsUrl))
    }

    static def getAllSponsors() {
        def jsonResponse = callApi(metaDataUrl())
        return jsonResponse['sponsors']
    }

    static def getAllTags() {
        def jsonResponse = callApi(metaDataUrl())
        return jsonResponse['tags']
    }

    static def getTagsForSponsor(sponsorID) {
        def url = HttpHelper.makeUrl(metaDataUrl(), makeEventFilter(['sponsors': sponsorID]))
        def jsonResponse = callApi(url)
        return jsonResponse['tags']
    }

    static def makeEventFilter(map) {
        return ['filter': map.collect { key, value -> "$key:$value" }.join(',')]
    }

    private static def callApi(URL url) {
        try {
            return new JsonSlurper().parse(url)
        } catch (JsonException j) {
            log.warn("Unable to parse JSON from URL: ${url.toString()}")
        }
    }

    private static def makeEvents(jsonStructure) {
        try {
            use(JsonOutput) {
                return jsonStructure.collect { detail -> new Gson().fromJson(detail.toJson(), Event.class) }
            }
        } catch (JsonException j) {
            log.warn("Unable to create events from json structure.")
        }
    }

    private static def makeEventsWithAndTags(jsonStructure, tags) {
        def tagsList = tags.collect { String tag ->
            tag.toLowerCase()
        }
        def events = makeEvents(jsonStructure)
        events = events.findResults { event ->
            def eventTagsList = event.tags.collect { String tag ->
                tag.toLowerCase()
            }
            eventTagsList.containsAll(tagsList) ? event : null
        }
        return events
    }
}