package edu.umich.lsa.peregrine.core.events

import java.util.Map;

public class Metadata {

    private Map<String, Integer> types;

    private Map<String, Integer> tags;

    private Map<String, Integer> locations;

    public Map<String, Integer> getTypes() {
        return types;
    }

    public void setTypes(Map<String, Integer> types) {
        this.types = types;
    }

    public Map<String, Integer> getTags() {
        return tags;
    }

    public void setTags(Map<String, Integer> tags) {
        this.tags = tags;
    }

    public Map<String, Integer> getLocations() {
        return locations;
    }

    public void setLocations(Map<String, Integer> locations) {
        this.locations = locations;
    }
}

