package edu.umich.lsa.peregrine.core.events;

//import helpers.JsoupHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import edu.umich.lsa.peregrine.core.events.*;

public class Event {
    private static int DESCRIPTION_LENGTH = 250;

    private static String GOOGLE_CALENDAR_URL = "https://www.google.com/calendar/render?action=TEMPLATE";
    private static String GOOGLE_CALENDAR_URL_END = "&trp=false&sprop&sprop=name:&sf=true&output=xml";

    private static String CAMPUS_MAP_URL = "http://campusinfo.umich.edu/campusmap/%s";

    private static String EVENTS_URL_BASE = "http://events.umich.edu/";

    private static String EVENT_ICAL_URL_TEMPLATE = EVENTS_URL_BASE + "event/%s/feed/ical";

    private static final Logger logger = LoggerFactory.getLogger(Event.class);

    private String datetime_modified;
    private String datetime_start;
    private String datetime_end;
    private int has_end_time;
    private String date_start;
    private String date_end;
    private String time_start;
    private String time_end;
    private String time_zone;
    private String event_title;
    private String occurrence_title;
    private String combined_title;
    private String event_subtitle;
    private String event_type;
    private String event_type_id;
    private String description;
    private String occurrence_notes;
    private String guid;
    private String permalink;
    private String building_id;
    private String building_name;
    private String room;
    private String location_name;
    private String campus_maps_id;
    private String cost;
    private List<String> tags;
    private String website;
    private List<Sponsor> sponsors;
    private String image_url;
    private Image styled_images;
    private int occurrence_count;
    private int occurrence_id;
    private String notes;
    private List<Event> occurrences;
    private int event_id;
    private int first_occurrence;
    private String id;
    private List<Link> links;
    private int has_livestream;
    private String livestream_type;
    private String livestream_id;
    private String livestream_password;
    private String livestream_link;


    public int getHas_livestream() { return has_livestream; }

    public void setHas_livestream(int has_livestream) { this.has_livestream = has_livestream; }

    public String getLivestream_type() { return livestream_type; }

    public void setLivestream_type(String livestream_type) {  this.livestream_type = livestream_type; }

    public String getLivestream_id() { return livestream_id; }

    public void setLivestream_id(String livestream_id) { this.livestream_id = livestream_id; }

    public String getLivestream_password() { return livestream_password; }

    public void setLivestream_password(String livestream_password) { this.livestream_password = livestream_password; }

    public String getLivestream_link() { return livestream_link; }

    public void setLivestream_link(String livestream_link) { this.livestream_link = livestream_link; }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventIdWithOccurrence()
    {
        return id;
    }

    public int getFirst_occurrence() {
        return first_occurrence;
    }

    public void setFirst_occurrence(int first_occurrence) {
        this.first_occurrence = first_occurrence;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getOccurrence_id() {
        return occurrence_id;
    }

    public void setOccurrence_id(int occurrence_id) {
        this.occurrence_id = occurrence_id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Event> getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(List<Event> occurrences) {
        this.occurrences = occurrences;
    }

    public int getOccurrence_count() {
        return occurrence_count;
    }

    public void setOccurrence_count(int occurrence_count) {
        this.occurrence_count = occurrence_count;
    }

    public String getDatetime_modified() {
        return datetime_modified;
    }
    public void setDatetime_modified(String datetime_modified) {
        this.datetime_modified = datetime_modified;
    }
    public String getDatetime_start() {
        return datetime_start;
    }
    public void setDatetime_start(String datetime_start) {
        this.datetime_start = datetime_start;
    }
    public String getDatetime_end() {
        return datetime_end;
    }
    public void setDatetime_end(String datetime_end) {
        this.datetime_end = datetime_end;
    }
    public int getHas_end_time() {
        return has_end_time;
    }
    public void setHas_end_time(int has_end_time) {
        this.has_end_time = has_end_time;
    }
    public String getDate_start() {
        return date_start;
    }
    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }
    public String getDate_end() {
        return date_end;
    }
    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }
    public String getTime_start() {
        return time_start;
    }
    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }
    public String getTime_end() {
        return time_end;
    }
    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }
    public String getTime_zone() {
        return time_zone;
    }
    public void setTime_zone(String time_zone) {
        this.time_zone = time_zone;
    }
    public String getEvent_title() {
//        return JsoupHelper.cleanEventTags(JsoupHelper.changeLinefeedToBr(this.event_title));
        return this.event_title;
    }
    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }
    public String getOccurrence_title() {
        return occurrence_title;
    }
    public void setOccurrence_title(String occurrence_title) {
        this.occurrence_title = occurrence_title;
    }
    public String getCombined_title() {
//        return JsoupHelper.removeAllHtmlTags(this.combined_title);
        return this.combined_title;
    }
    public void setCombined_title(String combined_title) {
        this.combined_title = combined_title;
    }
    public String getEvent_subtitle() {
//        return JsoupHelper.cleanEventTags(JsoupHelper.changeLinefeedToBr(this.event_subtitle));
        return this.event_subtitle;
    }
    public void setEvent_subtitle(String event_subtitle) {
        this.event_subtitle = event_subtitle;
    }
    public String getEvent_type() {
        return event_type;
    }
    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }
    public String getEvent_type_id() {
        return event_type_id;
    }
    public void setEvent_type_id(String event_type_id) {
        this.event_type_id = event_type_id;
    }
    public String getDescription() {
//        return JsoupHelper.cleanEventTags(JsoupHelper.changeLinefeedToBr(this.description));
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getOccurrence_notes() {
        return occurrence_notes;
    }
    public void setOccurrence_notes(String occurrence_notes) {
        this.occurrence_notes = occurrence_notes;
    }
    public String getGuid() {
        return guid;
    }
    public void setGuid(String guid) {
        this.guid = guid;
    }
    public String getPermalink() {
        return permalink;
    }
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }
    public String getBuilding_id() {
        return building_id;
    }
    public void setBuilding_id(String building_id) {
        this.building_id = building_id;
    }
    public String getBuilding_name() {
        return building_name;
    }
    public void setBuilding_name(String building_name) {
        this.building_name = building_name;
    }
    public String getRoom() {
        return room;
    }
    public String getLocation_name() {
        return location_name;
    }
    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
    public String getCampus_maps_id() {
        return campus_maps_id;
    }
    public void setCampus_maps_id(String campus_maps_id) {
        this.campus_maps_id = campus_maps_id;
    }
    public void setRoom(String room) {
        this.room = room;
    }
    public String getCost() {
        return cost;
    }
    public void setCost(String cost) {
        this.cost = cost;
    }
    public List<String> getTags() {
        return tags;
    }
    public void setTags(List<String> tags) {
        this.tags = tags;
    }
    public String getWebsite() {
        String url = website;
        url = url.replaceFirst("http://", "");
        url = url.replaceFirst("https://", "");
        url = url.replaceFirst("www.", "");
        return url;
    }
    public String getWebsiteUrl() {
        return website;
    }
    public void setWebsite(String url) {
        this.website = url;
    }
    public List<Sponsor> getSponsors() {
        return sponsors;
    }
    public void setSponsors(List<Sponsor> sponsors) {
        this.sponsors = sponsors;
    }
    public String getImage_url() {
        return image_url;
    }
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Image getStyled_images() {
        return styled_images;
    }

    public void setStyled_images(Image styled_images) {
        this.styled_images = styled_images;
    }

    public String getId() {
        return this.getGuid().substring(0, this.getGuid().indexOf("@"));
    }

    private String formatDate(DateFormat fmt, Date date) {
        if (fmt != null && date != null) {
            return fmt.format(date);
        } else {
            return "";
        }
    }

    public String getStartDateLongFormat() {
        DateFormat fmt = new SimpleDateFormat("EEEE, MMMM d, yyyy");
        return formatDate(fmt, this.getStartDate());
    }

    public Date getStartDate() {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return fmt.parse(this.getDate_start() + " " + this.getTime_start());
        } catch (ParseException e) {

        }

        return null;
    }

    public Date getEndDate() {

        if (this.has_end_time == 1) {
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                return fmt.parse(this.getDate_start() + " " + this.getTime_end());
            } catch (ParseException e) {

            }
        }

        return null;
    }

    public String getStartDateShort() {
        DateFormat fmt = new SimpleDateFormat("M/d");
        return formatDate(fmt, this.getStartDate());
    }

    public String getStartDateWithYear() {
        DateFormat fmt = new SimpleDateFormat("M/d/yyyy");
        return formatDate(fmt, this.getStartDate());
    }

    public String getStartDateMonth() {
        DateFormat fmt = new SimpleDateFormat("MMM");
        return formatDate(fmt, this.getStartDate());
    }

    public String getStartDateMonthAndDay() {
        DateFormat fmt = new SimpleDateFormat("LLLL");
        String month = formatDate(fmt, this.getStartDate());
        String day = getStartDateDay();
        return month + " " + day;
    }

    public String getStartDateDay() {
        DateFormat fmt = new SimpleDateFormat("dd");
        return formatDate(fmt, this.getStartDate());
    }

    public String getStartDateTime() {
        return this.getDateTime(this.getStartDate(), true);
    }

    public String getStartDateTimeLong() {
        return this.getDateTimeLong(this.getStartDate(), true);
    }

    public String getStartManual() {
        return this.getDateTimeLongForManualEvent(this.getStartDate(), true);
    }

    public String getStartDateTimeNoAMPM() {
        return this.getDateTime(this.getStartDate(), false);
    }

    public String getStartEndTime() {
        Boolean showAMPM = true;
        if (this.getAMPM(this.getStartDate()).equals(this.getAMPM(this.getEndDate()))) {
            showAMPM = false;
        }

        if (this.has_end_time == 1) {
            return this.getDateTime(this.getStartDate(), showAMPM) + "-" + this.getDateTime(this.getEndDate(), true);
        } else {
            return this.getDateTime(this.getStartDate(), true);
        }
    }

    public String getStartEndTimeLong() {
        Boolean showAMPM = true;
        if (this.getAMPM(this.getStartDate()).equals(this.getAMPM(this.getEndDate()))) {
            showAMPM = false;
        }

        if (this.has_end_time == 1) {
            return this.getDateTimeLong(this.getStartDate(), showAMPM) + "-" + this.getDateTimeLong(this.getEndDate(), true);
        } else {
            return this.getDateTimeLong(this.getStartDate(), true);
        }
    }

    public String getEndDateTime() {
        return this.getDateTime(this.getEndDate(), true);
    }

    public String getDateTime(Date date, Boolean showAMPM) {
        String AMPM = "";
        if (showAMPM) {
            AMPM = " a";
        }
        DateFormat minuteFormat = new SimpleDateFormat("m");
        String minute = formatDate(minuteFormat, date);

        if (minute.equals("0") || minute.equals("00")) {
            DateFormat hourFormat = new SimpleDateFormat("h" + AMPM);
            return formatDate(hourFormat, date);
        } else {
            DateFormat hourMinuteFormat = new SimpleDateFormat("h:mm" + AMPM);
            return formatDate(hourMinuteFormat, date);
        }
    }

    public String getDateTimeLong(Date date, Boolean showAMPM) {
        String AMPM = "";
        if (showAMPM) {
            AMPM = " a";
        }
        DateFormat hourMinuteFormat = new SimpleDateFormat("h:mm" + AMPM);
        return formatDate(hourMinuteFormat, date);
    }

    public String getDateTimeLongForManualEvent(Date date, Boolean showAMPM) {
        String AMPM = "";
        if (showAMPM) {
            AMPM = " a";
        }
        DateFormat hourMinuteFormat = new SimpleDateFormat("h:mm" + AMPM);
        hourMinuteFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        return formatDate(hourMinuteFormat, date);
    }

    public String getAMPM(Date date) {
        DateFormat format = new SimpleDateFormat("a");
        return formatDate(format, date);
    }

    public String getEndDateShort() {
        DateFormat fmt = new SimpleDateFormat("M/d");
        return formatDate(fmt, this.getEndDate());
    }

    public String getEndDateMonth() {
        DateFormat fmt = new SimpleDateFormat("MMM");
        return formatDate(fmt, this.getEndDate());
    }

    public String getEndDateDay() {
        DateFormat fmt = new SimpleDateFormat("dd");
        return formatDate(fmt, this.getEndDate());
    }

    public String getTrimmedDescription() {
        if (this.getDescription().length() > DESCRIPTION_LENGTH) {
            return this.getDescription().substring(0, DESCRIPTION_LENGTH - 3) + "...";
        } else {
            return this.getDescription();
        }
    }

    private String encode(String input) {
        try {
            return URLEncoder.encode(input, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return input.replaceAll(" ", "+").replaceAll("&", "%26").replaceAll("\\?", "%3F");
        }
    }

    public String getUTCDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime() - TimeZone.getTimeZone(this.time_zone).getOffset(date.getTime()));

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");

        return formatDate(format, cal.getTime());
    }

    public String getUTCStartDate() {
        return getUTCDate(this.getStartDate());
    }

    public String getUTCEndDate() {
        return getUTCDate(this.getEndDate());
    }

    public String getGoogleCalendarUrl() {
        StringBuilder sb = new StringBuilder(GOOGLE_CALENDAR_URL);

        if (this.event_title != null && !this.event_title.isEmpty()) {
            sb.append("&text=").append(encode(this.event_title));
        }

        if (this.datetime_start != null && !this.datetime_start.isEmpty()) {
            sb.append("&dates=").append(this.getUTCStartDate());
            if (this.datetime_end != null && !this.datetime_end.isEmpty()) {
                sb.append("/").append(this.getUTCEndDate());
            }
        }

        if (this.description != null && !this.description.isEmpty()) {
            sb.append("&details=").append(encode(this.getTrimmedDescription()));
        }

        if (this.building_name != null && !this.building_name.isEmpty()) {
            sb.append("&location=").append(encode(this.building_name));
        }

        sb.append(GOOGLE_CALENDAR_URL_END);

        logger.debug("Google URL: " + sb.toString());

        return sb.toString();
    }

    public String getIcalUrl() {
        return String.format(EVENT_ICAL_URL_TEMPLATE, this.getId());
    }

    public String getCampusMapUrl() {
        return String.format(CAMPUS_MAP_URL, this.getCampus_maps_id());
    }
}
