package edu.umich.lsa.peregrine.core.helpers

import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.HttpStatus
import org.apache.commons.httpclient.methods.GetMethod
import org.apache.commons.io.IOUtils

class HttpHelper {
    static String appendToUrlString(String base, String strToAppend) {
        if (base.endsWith('/')) {
            if (strToAppend.startsWith('/')) {
                return [base, strToAppend.substring(1)].join('')
            } else {
                return [base, strToAppend].join('')
            }
        } else {
            if (strToAppend.startsWith('/')) {
                return [base, strToAppend].join('')
            } else {
                return [base, strToAppend].join('/')
            }
        }
    }

    public static String GetHttpResponse(String url) {
        String response = ""

        final HttpClient client = new HttpClient()
        final GetMethod method = new GetMethod(url)

        client.getHttpConnectionManager().getParams().setConnectionTimeout(10000)
        client.getHttpConnectionManager().getParams().setSoTimeout(10000)

        try {
            final int statusCode = client.executeMethod(method)

            if (statusCode == HttpStatus.SC_OK) {
                final InputStream inputStream = method.getResponseBodyAsStream()
                response = IOUtils.toString(inputStream, method.getResponseCharSet())
            }
        } catch (Exception e) {

        } finally {
            method.releaseConnection();
        }

        return response;
    }

    static URL makeUrl(base, params=[]) {
        return makeUrlString(base, params).toURL()
    }

    static String makeUrlString(base, params=[]) {
        def metaUrl = { separator ->
            return ["$base", params.collect { it }.join('&')].findAll().join(separator)
        }
        if ("$base".contains('?')) {
//            Already have query params started, so just add the new params with '&'
            return metaUrl('&')
        } else {
//            If there are parameters, join them together with '&', then join that with the base url with a '?'
//            Will just return "$base" if there are no params
            return metaUrl('?')
        }
    }

    static String stripQueryParameters(String str) {
        return str.replaceAll(/\?.*/, '')
    }
}
