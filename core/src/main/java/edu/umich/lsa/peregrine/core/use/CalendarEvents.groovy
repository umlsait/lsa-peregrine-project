package edu.umich.lsa.peregrine.core.use

import edu.umich.lsa.peregrine.core.events.Event

import javax.script.Bindings
import com.google.gson.*;
import edu.umich.lsa.peregrine.core.helpers.HttpHelper

class CalendarEvents extends Events {
    String rangeStart
    String rangeEnd
    String cosponsor
    List categoryTags

    protected static final String MULTI_DAY_TITLE = "Multi-day Events";

    protected Map<String, EventGroup> eventGroups;


    public void setRangeStart(String rangeStartParam) {
        this.rangeStart = this.getRequest().getParameter(rangeStartParam);
    }

    public void setRangeEnd(String rangeEndParam) {
        this.rangeEnd = this.getRequest().getParameter(rangeEndParam);
    }

    public void setEventGroups(Map<String, EventGroup> eventGroups) {
        this.eventGroups = eventGroups;
    }

    public void setCategoryTags(String categoryTagsProperty) {
        this.categoryTags = this.getProperties().get(categoryTagsProperty, []);
    }


    @Override
    void init(Bindings bindings) {
        super.init(bindings)
        this.categoryTags = this.getProperties().get("categoryTags", []);
        this.rangeStart = this.getRequest().getParameter("start");
        this.rangeEnd = this.getRequest().getParameter("end");

        if (this.getUrl() != null && !this.getUrl().isEmpty()) {
            this.processEvents();
        }
    }

    protected void processEvents() {

        eventGroups = new LinkedHashMap<String, EventGroup>();

        String json = "";
        try {
            String calUrl = this.getFullUrl();
            log.debug(calUrl);
            json = HttpHelper.GetHttpResponse(calUrl);
            log.debug(json);
            if(json.length() <4){ // [ ], or  []
                log.info("No Featured Event to Process, json="+json);
                return;
            }
            JsonParser parser = new JsonParser();
            JsonArray data = parser.parse(json).getAsJsonArray();

            Gson gson = new GsonBuilder().create();

            for (JsonElement entry : data) {
                Event event = gson.fromJson(entry, Event.class);
                if (eventGroups.containsKey(event.getDate_start())) {
                    eventGroups.get(event.getDate_start()).addEvent(event);
                } else {
                    eventGroups.put(event.getDate_start(), new EventGroup(event.getStartDateLongFormat(), false, event));
                }
            }

        } catch(Exception e) {
            log.error(e.toString());
        }
    }


    protected String getFullUrl() {
        String url;
        if(cosponsor != null && !cosponsor.isEmpty()){
            url = this.getUrl(cosponsor);
        } else {
            url = getUrl();
        }
        url = url.replace("week/json", "list/json");

        if (this.getCategoryTags().length > 0) {
            return String.format("%s&filter=tags:%s&range=%sto%s", url, this.getTagQuery(this.getCategoryTags()), this.rangeStart, this.rangeEnd);
        } else {
            return String.format("%s&range=%sto%s", url, this.rangeStart, this.rangeEnd);
        }
    }

    public List<EventGroup> getEventGroups() {
        return eventGroups != null? new ArrayList<EventGroup>(eventGroups.values()) : null;
    }

    public String[] getCategoryTags() {
        return this.categoryTags;
    }

    public class EventGroup {
        private String title;
        private List<Event> events = new ArrayList<Event>();
        private Boolean isMultiDay = false;

        public EventGroup(String title, Boolean isMultiDay, Event event) {
            this.title = title;
            this.isMultiDay = isMultiDay;
            this.events.add(event);
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Boolean getIsMultiDay() {
            return isMultiDay;
        }

        public void setIsMultiDay(Boolean isMultiDay) {
            this.isMultiDay = isMultiDay;
        }

        public List<Event> getEvents() {
            return events;
        }

        public void addEvent(Event event) {
            events.add(event);
        }

        public String getStartDate() {

            return events.get(0).getDate_start();
        }
    }
}
