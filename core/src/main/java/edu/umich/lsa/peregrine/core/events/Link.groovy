package edu.umich.lsa.peregrine.core.events

/**
 * Created by anantas on 1/27/17.
 */
class Link {
    String type;
    String url;
    String title;
}