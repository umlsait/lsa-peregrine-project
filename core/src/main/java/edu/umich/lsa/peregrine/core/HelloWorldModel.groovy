package edu.umich.lsa.peregrine.core

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

/**
 * Service for greeting the world
 */
@Model(adaptables=[Resource.class])
public class HelloWorldModel {

    def property1 = "hard coded prop"

    public String getMessage(){
        return "Hello World!";
    }
}
