package edu.umich.lsa.peregrine.core.events

public class Sponsor {
    private String group_name;
    private String group_id;
    private String website;

    String getGroup_name() {
        return group_name;
    }
    void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
    String getGroup_id() {
        return group_id;
    }
    void setGroup_id(String group_id) {
        this.group_id = group_id;
    }
    String getWebsite() {
        return website;
    }
    void setWebsite(String website) {
        this.website = website;
    }
}